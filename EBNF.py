import string
class Rule:
    type = ''
    content = []

    def __init__(self, in_text):
        text = in_text[:].strip()
        self.type = 'loop'
        if text[0] == '"' and text[-1] == '"':
            self.type = 'terminal'
            self.content = text
        elif text[0] == '[' and text[-1] == ']':
            self.type = 'option'
        elif text[0] == '(' and text[-1] == ')':
            self.type = 'group'
        elif text[0] == '{' and text[-1] == '}':
            self.type = 'loop'
        else:
            self.type = 'non-terminal'
        self.content = self.parse(text)

    def parse(self, text):
        content = []
        seq = []
        terminalSingle = False
        terminalDouble = False
        group = 0
        option = 0
        loop = 0
        acc = ''
        for symbol in text:
            if not terminalDouble and not terminalSingle:
                if symbol == '"':
                    terminalDouble = True
                elif symbol == "'":
                    terminalSingle = True
                elif symbol == '(':
                    group += 1
                elif symbol == '[':
                    option += 1
                elif symbol == '{':
                    loop += 1
                elif symbol == ')':
                    group -= 1
                elif symbol == ']':
                    option -= 1
                elif symbol == '}':
                    loop -= 1
                elif ( group == option == loop == 0 ):
                    if symbol == ',':
                        seq.append(Rule(acc.strip()))
                        acc = ''
                        continue
                    elif symbol == '|':
                        seq.append(Rule(acc.strip()))
                        content.append(seq)
                        acc = ''
                        seq = []
                        continue
            elif terminalDouble and symbol == '"':
                terminalDouble = False
            elif terminalSingle and symbol == "'":
                terminalSingle = False
            acc += symbol
        return content

    def __str__(self):
        prefix = ''
        suffix = ''
        if self.isTerminal():
            prefix = '"'
            suffix = '"'
        elif self.isLoop():
            prefix = '{'
            suffix = '}'
        elif self.isOption():
            prefix = '['
            suffix = ']'
        elif self.isGroup():
            prefix = '('
            suffix = ')'
        return prefix + " " + str(self.content) + " " + suffix

    def isNonTerminal(self):
        return self.type == 'non-terminal'

    def isTerminal(self):
        return self.type == "terminal"

    def isLoop(self):
        return self.type == "loop"

    def isOption(self):
        return self.type == "option"

    def isGroup(self):
        return self.type == "group"

class EBNF:
    rules = dict()
    main_rule = ''
    def __init__(self, main_rule, rules=None):
        if rules:
            self.parseEBNF(main_rule, rules)
        else:
            f = open(main_rule)
            ebnfRules = ''
            mainRule = ''
            for line in f:
                if not mainRule:
                    mainRule = line.strip()
                else:
                    ebnfRules += line.strip()
            print(ebnfRules)
            self.parseEBNF(mainRule, ebnfRules)

    def parseEBNF(self, main_rule, rules):
        self.main_rule = main_rule
        self.parseRule(rules)

    def __str__(self):
        res_str = 'Main rule: ' + self.main_rule + '\nEBNF rules:\n'
        for key, value in self.rules.items():
            res_str += key + ' = ' + ' | '.join([', '.join(part) for part in value]) + ' ; \n'
        return res_str



    def checkTerminal(self, rule, phrase):
        return rule.strip('"') == phrase

    def checkOption(self, rule, phrase):
        if phrase == '':
            return True
        innerPart = self.parseRule(rule.lstrip('[').rstrip(']').strip())
        return self.checkChoice(innerPart, phrase)

    def checkGroup(self, rule, phrase):
        innerPart = self.parseRule(rule.lstrip('(').rstrip(')').strip())
        return self.checkChoice(innerPart, phrase)

    def checkLoop(self, rule, phrase):
        if phrase == '':
            return True
        innerPart = self.parseRule(rule[:].lstrip('{').rstrip('}').strip())
        for seq in innerPart:
            seq.append(rule)
        return self.checkChoice(innerPart, phrase)

    def checkRule(self, rule, phrase):
        if self.isTerminal(rule):
            return self.checkTerminal(rule, phrase)
        elif self.isGroup(rule):
            return self.checkGroup(rule, phrase)
        elif self.isOption(rule):
            return self.checkOption(rule, phrase)
        elif self.isLoop(rule):
            return self.checkLoop(rule, phrase)
        else:
            return self.checkChoice(self.rules[rule], phrase)

    def checkSequence(self, seq, phrase):
        current_rule = seq[0]
        if len(seq) == 1:
            return self.checkRule(current_rule, phrase)
        else:
            excess = seq[1:]
            tmp = ''
            for i in range(len(phrase)):
                if self.checkRule(current_rule, tmp) and self.checkSequence(excess, phrase[i:]):
                    return True
                tmp += phrase[i]
            return self.checkRule(current_rule, tmp) and self.checkSequence(excess, '')

    def checkChoice(self, seq, phrase):
        for rule in seq:
            if self.checkSequence(rule, phrase):
                return True
        return False

    def validate(self, phrase):
        return self.checkRule(self.main_rule, phrase)


def test1():
    ebnf = EBNF('digit', 'digit = "0" | digit excluding zero ; digit excluding zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;')
    print(ebnf)
    for i in range(-20, 21):
        print(i, ebnf.validate(str(i)))
    for i in string.ascii_letters:
        print(i, ebnf.validate(i))

def test2():
    ebnf = EBNF('twelve thousand two hundred one', 'twelve thousand two hundred one = twelve, two hundred one ; twelve = "1", "2" ; two hundred one = "2", "0", "1" ; three hundred twelve = "3", twelve ;')
    print(ebnf)
    for i in range(21):
        print(i, ebnf.validate(str(i)))
    for i in string.ascii_letters:
        print(i, ebnf.validate(i))
    for i in (12, 201, 312, 12201):
        print(i, ebnf.validate(str(i)))

def test3():
    ebnf = EBNF('natural number', 'natural number = digit excluding zero, { digit } ; digit = "0" | digit excluding zero ; digit excluding zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;')
    print(ebnf)
    for i in range(-20, 21):
        print(i, ebnf.validate(str(i)))
    for i in string.ascii_letters:
        print(i, ebnf.validate(i))

def test4():
    ebnf = EBNF('integer', 'integer = "0" | [ "-" ], natural number ; natural number = digit excluding zero, { digit } ; digit = "0" | digit excluding zero ; digit excluding zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;')
    print(ebnf)
    for i in range(-20, 21):
        print(i, ebnf.validate(str(i)))
    for i in string.ascii_letters:
        print(i, ebnf.validate(i))

def testEbnf():
    ebnf = EBNF('grammarEBNF')
    print(ebnf)
    print(ebnf.validate('integer = "0" | [ "-" ], natural number ; natural number = digit excluding zero, { digit } ; digit = "0" | digit excluding zero ; digit excluding zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;'))



rule = Rule('identifier | terminal | "[" , rhs , "]" | "{" , rhs , "}" | "(" , rhs , ")" | rhs , "|" , rhs | rhs , "," , rhs ;')

print(rule)
# test1()
# test2()
# test3()
# test4()
#testEbnf()#